<?php

define('DIR_MODIFICATION', '/var/www/html/haidainc/system/modification/');

// HTTP
define('HTTP_SERVER', 'http://localhost/haidainc/admin/');
define('HTTP_CATALOG', 'http://localhost/haidainc/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/haidainc/admin/');
define('HTTPS_CATALOG', 'http://localhost/haidainc/');

// DIR
define('DIR_APPLICATION', '/var/www/html/haidainc/admin/');
define('DIR_IMAGE', '/var/www/html/haidainc/image/');
define('DIR_SYSTEM', '/var/www/html/haidainc/system/');
define('DIR_STORAGE', '/var/www/html/haidainc/storage/');
define('DIR_CATALOG', '/var/www/html/haidainc/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'dhaka1');
define('DB_DATABASE', 'haidainc');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
