<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com
$_['heading_title']     = 'عربة التسوق';
// Text
$_['text_items']    = '%s بند';
$_['cart_title']     = 'عربتي';
$_['text_empty']    = 'سلة الشراء فارغة !';
$_['text_cart']     = 'معاينة السلة';
$_['text_checkout'] = 'إنهاء الطلب';
$_['text_recurring']  = 'ملف الدفع';
$_['text_title']      = 'عربتي';