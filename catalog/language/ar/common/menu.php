<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Text
$_['text_all'] = 'عرض الكل';
$_['name'] = 'الصفحة الرئيسية';
$_['text_blog'] = 'مدونة';
$_['text_contactus'] = 'اتصل';
$_['text_aboutus'] = 'معلومات عنا';
$_['text_manufacturer'] = 'علامة تجارية';
$_['text_voucher'] = 'شهادات الهدية';
$_['text_special']      = 'العروض الخاصة';

