<?php
// Heading
$_['heading_title']     = 'النشرة الإخبارية';

// Entry
$_['entry_email']          = 'أدخل عنوان بريدك الالكتروني..';

// Email
$_['email_subject']       = 'أهلا بك';
$_['email_content']       = 'شكرا لك على الاشتراك';

// Button
$_['email_button']       = 'الاشتراك';

//Description
$_['heading_desc']     = 'هل تريد إرسال أخبار خاصة إلى صندوق الوارد الخاص بك؟';