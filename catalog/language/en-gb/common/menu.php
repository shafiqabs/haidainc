<?php
// Text
$_['text_all'] = 'Show All';

$_['name'] = 'home';
$_['text_closeout'] = 'Closeout';
$_['text_new_arrivals'] = 'New Arrivals';
$_['text_best_sellers'] = 'Best Sellers';
$_['text_clearance'] = 'Clearance';
$_['text_blog'] = 'blog';
$_['text_sitemap'] = 'sitemap';
$_['text_contactus'] = 'contact';
$_['text_aboutus'] = 'about us';
$_['text_manufacturer'] = 'Brand';
$_['text_voucher'] = 'Gift Certificates';
$_['text_special']      = 'Specials';