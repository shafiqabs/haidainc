<?php
// Heading
$_['heading_title']     = 'Catalogs';

// Text
$_['text_account']      = 'Account';
$_['text_downloads']    = 'Downloads';
$_['text_empty']        = 'You have not made any previous downloadable orders!';

// Column
$_['column_order_id']   = 'ID';
$_['column_name']       = 'Name';
$_['column_size']       = 'Size';
$_['column_date_added'] = 'Date Added';