<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* report/online.twig */
class __TwigTemplate_3d0826ebe8d5ab6fca5df5ec5b193429fafec387eb81d53d6a689f631ecf4cfa extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"float-right\">
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_filter"] ?? null);
        echo "\" onclick=\"\$('#filter-online').toggleClass('d-none');\" class=\"btn btn-light d-md-none\"><i class=\"fas fa-filter\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["refresh"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_refresh"] ?? null);
        echo "\" class=\"btn btn-light\"><i class=\"fas fa-sync\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ol class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "          <li class=\"breadcrumb-item\"><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ol>
    </div>
  </div>
  <div class=\"container-fluid\">
    <div class=\"row\">
      <div id=\"filter-online\" class=\"col-md-3 col-sm-12 order-md-9 d-none d-md-block mb-3\">
        <div class=\"card\">
          <div class=\"card-header\"><i class=\"fas fa-filter\"></i> ";
        // line 20
        echo ($context["text_filter"] ?? null);
        echo "</div>
          <div class=\"card-body\">
            <div class=\"form-group\">
              <label for=\"input-ip\" class=\"col-form-label\">";
        // line 23
        echo ($context["entry_ip"] ?? null);
        echo "</label> <input type=\"text\" name=\"filter_ip\" value=\"";
        echo ($context["filter_ip"] ?? null);
        echo "\" id=\"input-ip\" placeholder=\"";
        echo ($context["entry_ip"] ?? null);
        echo "\" i class=\"form-control\"/>
            </div>
            <div class=\"form-group\">
              <label class=\"col-form-label\">";
        // line 26
        echo ($context["entry_customer"] ?? null);
        echo "</label> <input type=\"text\" name=\"filter_customer\" value=\"";
        echo ($context["filter_customer"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_customer"] ?? null);
        echo "\" id=\"input-customer\" class=\"form-control\"/>
            </div>
            <div class=\"form-group text-right\">
              <button type=\"button\" id=\"button-filter\" class=\"btn btn-light\"><i class=\"fas fa-filter\"></i> ";
        // line 29
        echo ($context["button_filter"] ?? null);
        echo "</button>
            </div>
          </div>
        </div>
      </div>
      <div class=\"col-md-9 col-sm-12\">
        <div class=\"card\">
          <div class=\"card-header\"><i class=\"fas fa-list\"></i> ";
        // line 36
        echo ($context["text_list"] ?? null);
        echo "</div>
          <div class=\"card-body\">
            <div class=\"table-responsive\">
              <table class=\"table table-bordered table-hover\">
                <thead>
                  <tr>
                    <td class=\"text-left\">";
        // line 42
        echo ($context["column_ip"] ?? null);
        echo "</td>
                    <td class=\"text-left\">";
        // line 43
        echo ($context["column_customer"] ?? null);
        echo "</td>
                    <td class=\"text-left\">";
        // line 44
        echo ($context["column_url"] ?? null);
        echo "</td>
                    <td class=\"text-left\">";
        // line 45
        echo ($context["column_referer"] ?? null);
        echo "</td>
                    <td class=\"text-left\">";
        // line 46
        echo ($context["column_date_added"] ?? null);
        echo "</td>
                    <td class=\"text-right\">";
        // line 47
        echo ($context["column_action"] ?? null);
        echo "</td>
                  </tr>
                </thead>
                <tbody>
                  ";
        // line 51
        if (($context["customers"] ?? null)) {
            // line 52
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["customers"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["customer"]) {
                // line 53
                echo "                      <tr>
                        <td class=\"text-left\"><a href=\"//whatismyipaddress.com/ip/";
                // line 54
                echo twig_get_attribute($this->env, $this->source, $context["customer"], "ip", [], "any", false, false, false, 54);
                echo "\" target=\"_blank\">";
                echo twig_get_attribute($this->env, $this->source, $context["customer"], "ip", [], "any", false, false, false, 54);
                echo "</a></td>
                        <td class=\"text-left\">";
                // line 55
                echo twig_get_attribute($this->env, $this->source, $context["customer"], "customer", [], "any", false, false, false, 55);
                echo "</td>
                        <td class=\"text-left\"><a href=\"";
                // line 56
                echo twig_get_attribute($this->env, $this->source, $context["customer"], "url", [], "any", false, false, false, 56);
                echo "\" target=\"_blank\" rel=\"noreferrer\">";
                echo twig_join_filter(twig_split_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "url", [], "any", false, false, false, 56), "", 30), "<br/>");
                echo "</a></td>
                        <td class=\"text-left\">";
                // line 57
                if (twig_get_attribute($this->env, $this->source, $context["customer"], "referer", [], "any", false, false, false, 57)) {
                    echo "<a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["customer"], "referer", [], "any", false, false, false, 57);
                    echo "\" target=\"_blank\">";
                    echo twig_join_filter(twig_split_filter($this->env, twig_get_attribute($this->env, $this->source, $context["customer"], "referer", [], "any", false, false, false, 57), "", 30), "<br/>");
                    echo "</a>";
                }
                echo "</td>
                        <td class=\"text-left\">";
                // line 58
                echo twig_get_attribute($this->env, $this->source, $context["customer"], "date_added", [], "any", false, false, false, 58);
                echo "</td>
                        <td class=\"text-right\">";
                // line 59
                if (twig_get_attribute($this->env, $this->source, $context["customer"], "customer_id", [], "any", false, false, false, 59)) {
                    echo "<a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["customer"], "edit", [], "any", false, false, false, 59);
                    echo "\" data-toggle=\"tooltip\" title=\"";
                    echo ($context["button_edit"] ?? null);
                    echo "\" class=\"btn btn-primary\"><i class=\"fas fa-pencil-alt\"></i></a>";
                } else {
                    // line 60
                    echo "                            <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                    echo ($context["button_edit"] ?? null);
                    echo "\" class=\"btn btn-primary\" disabled=\"disabled\"><i class=\"fas fa-pencil-alt\"></i></button>
                          ";
                }
                // line 61
                echo "</td>
                      </tr>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "                  ";
        } else {
            // line 65
            echo "                    <tr>
                      <td class=\"text-center\" colspan=\"6\">";
            // line 66
            echo ($context["text_no_results"] ?? null);
            echo "</td>
                    </tr>
                  ";
        }
        // line 69
        echo "                </tbody>

              </table>
            </div>
            <div class=\"row\">
              <div class=\"col-sm-6 text-left\">";
        // line 74
        echo ($context["pagination"] ?? null);
        echo "</div>
              <div class=\"col-sm-6 text-right\">";
        // line 75
        echo ($context["results"] ?? null);
        echo "</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type=\"text/javascript\"><!--
\$('#button-filter').on('click', function() {
\tvar url = 'index.php?route=report/online&user_token=";
        // line 85
        echo ($context["user_token"] ?? null);
        echo "';

\tvar filter_customer = \$('input[name=\\'filter_customer\\']').val();

\tif (filter_customer) {
\t\turl += '&filter_customer=' + encodeURIComponent(filter_customer);
\t}

\tvar filter_ip = \$('input[name=\\'filter_ip\\']').val();

\tif (filter_ip) {
\t\turl += '&filter_ip=' + encodeURIComponent(filter_ip);
\t}

\tlocation = url;
});
//--></script>
<script type=\"text/javascript\"><!--
\$('input[name=\\'filter_customer\\']').autocomplete({
\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=customer/customer/autocomplete&user_token=";
        // line 106
        echo ($context["user_token"] ?? null);
        echo "&filter_name=' + encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\tvalue: item['customer_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_customer\\']').val(item['label']);
\t}
});
//--></script>
";
        // line 123
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "report/online.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  299 => 123,  279 => 106,  255 => 85,  242 => 75,  238 => 74,  231 => 69,  225 => 66,  222 => 65,  219 => 64,  211 => 61,  205 => 60,  197 => 59,  193 => 58,  183 => 57,  177 => 56,  173 => 55,  167 => 54,  164 => 53,  159 => 52,  157 => 51,  150 => 47,  146 => 46,  142 => 45,  138 => 44,  134 => 43,  130 => 42,  121 => 36,  111 => 29,  101 => 26,  91 => 23,  85 => 20,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "report/online.twig", "");
    }
}
