<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* catalog/option_form.twig */
class __TwigTemplate_51edff5b2202e0b9172f369f2d8558a1d5fa1eedd6ca3ae8b2ed140e43b637ca extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"float-right\">
        <button type=\"submit\" form=\"form-option\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fas fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-light\"><i class=\"fas fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ol class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "          <li class=\"breadcrumb-item\"><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ol>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if (($context["error_warning"] ?? null)) {
            // line 18
            echo "      <div class=\"alert alert-danger alert-dismissible\"><i class=\"fas fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
      </div>
    ";
        }
        // line 22
        echo "    <div class=\"card\">
      <div class=\"card-header\"><i class=\"fas fa-pencil-alt\"></i> ";
        // line 23
        echo ($context["text_form"] ?? null);
        echo "</div>
      <div class=\"card-body\">
        <form action=\"";
        // line 25
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-option\">
          <fieldset>
            <legend>";
        // line 27
        echo ($context["text_option"] ?? null);
        echo "</legend>
            <div class=\"form-group row required\">
              <label class=\"col-sm-2 col-form-label\">";
        // line 29
        echo ($context["entry_name"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 32
            echo "                  <div class=\"input-group\">
                    <div class=\"input-group-prepend\">
                      <span class=\"input-group-text\"><img src=\"language/";
            // line 34
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 34);
            echo "/";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 34);
            echo ".png\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 34);
            echo "\"/></span>
                    </div>
                    <input type=\"text\" name=\"option_description[";
            // line 36
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 36);
            echo "][name]\" value=\"";
            echo (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["option_description"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 36)] ?? null) : null)) ? (twig_get_attribute($this->env, $this->source, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["option_description"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 36)] ?? null) : null), "name", [], "any", false, false, false, 36)) : (""));
            echo "\" placeholder=\"";
            echo ($context["entry_name"] ?? null);
            echo "\" class=\"form-control\"/>
                  </div>
                  ";
            // line 38
            if ((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["error_name"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 38)] ?? null) : null)) {
                // line 39
                echo "                    <div class=\"invalid-tooltip\">";
                echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["error_name"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 39)] ?? null) : null);
                echo "</div>
                  ";
            }
            // line 41
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</div>
            </div>
            <div class=\"form-group row\">
              <label for=\"input-type\" class=\"col-sm-2 col-form-label\">";
        // line 44
        echo ($context["entry_type"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"type\" id=\"input-type\" class=\"form-control\">
                  <optgroup label=\"";
        // line 47
        echo ($context["text_choose"] ?? null);
        echo "\">
                    ";
        // line 48
        if ((($context["type"] ?? null) == "select")) {
            // line 49
            echo "                      <option value=\"select\" selected=\"selected\">";
            echo ($context["text_select"] ?? null);
            echo "</option>
                    ";
        } else {
            // line 51
            echo "                      <option value=\"select\">";
            echo ($context["text_select"] ?? null);
            echo "</option>
                    ";
        }
        // line 53
        echo "                    ";
        if ((($context["type"] ?? null) == "radio")) {
            // line 54
            echo "                      <option value=\"radio\" selected=\"selected\">";
            echo ($context["text_radio"] ?? null);
            echo "</option>
                    ";
        } else {
            // line 56
            echo "                      <option value=\"radio\">";
            echo ($context["text_radio"] ?? null);
            echo "</option>
                    ";
        }
        // line 58
        echo "                    ";
        if ((($context["type"] ?? null) == "checkbox")) {
            // line 59
            echo "                      <option value=\"checkbox\" selected=\"selected\">";
            echo ($context["text_checkbox"] ?? null);
            echo "</option>
                    ";
        } else {
            // line 61
            echo "                      <option value=\"checkbox\">";
            echo ($context["text_checkbox"] ?? null);
            echo "</option>
                    ";
        }
        // line 63
        echo "                  </optgroup>
                  <optgroup label=\"";
        // line 64
        echo ($context["text_input"] ?? null);
        echo "\">
                    ";
        // line 65
        if ((($context["type"] ?? null) == "text")) {
            // line 66
            echo "                      <option value=\"text\" selected=\"selected\">";
            echo ($context["text_text"] ?? null);
            echo "</option>
                    ";
        } else {
            // line 68
            echo "                      <option value=\"text\">";
            echo ($context["text_text"] ?? null);
            echo "</option>
                    ";
        }
        // line 70
        echo "                    ";
        if ((($context["type"] ?? null) == "textarea")) {
            // line 71
            echo "                      <option value=\"textarea\" selected=\"selected\">";
            echo ($context["text_textarea"] ?? null);
            echo "</option>
                    ";
        } else {
            // line 73
            echo "                      <option value=\"textarea\">";
            echo ($context["text_textarea"] ?? null);
            echo "</option>
                    ";
        }
        // line 75
        echo "                  </optgroup>
                  <optgroup label=\"";
        // line 76
        echo ($context["text_file"] ?? null);
        echo "\">
                    ";
        // line 77
        if ((($context["type"] ?? null) == "file")) {
            // line 78
            echo "                      <option value=\"file\" selected=\"selected\">";
            echo ($context["text_file"] ?? null);
            echo "</option>
                    ";
        } else {
            // line 80
            echo "                      <option value=\"file\">";
            echo ($context["text_file"] ?? null);
            echo "</option>
                    ";
        }
        // line 82
        echo "                  </optgroup>
                  <optgroup label=\"";
        // line 83
        echo ($context["text_date"] ?? null);
        echo "\">
                    ";
        // line 84
        if ((($context["type"] ?? null) == "date")) {
            // line 85
            echo "                      <option value=\"date\" selected=\"selected\">";
            echo ($context["text_date"] ?? null);
            echo "</option>
                    ";
        } else {
            // line 87
            echo "                      <option value=\"date\">";
            echo ($context["text_date"] ?? null);
            echo "</option>
                    ";
        }
        // line 89
        echo "                    ";
        if ((($context["type"] ?? null) == "time")) {
            // line 90
            echo "                      <option value=\"time\" selected=\"selected\">";
            echo ($context["text_time"] ?? null);
            echo "</option>
                    ";
        } else {
            // line 92
            echo "                      <option value=\"time\">";
            echo ($context["text_time"] ?? null);
            echo "</option>
                    ";
        }
        // line 94
        echo "                    ";
        if ((($context["type"] ?? null) == "datetime")) {
            // line 95
            echo "                      <option value=\"datetime\" selected=\"selected\">";
            echo ($context["text_datetime"] ?? null);
            echo "</option>
                    ";
        } else {
            // line 97
            echo "                      <option value=\"datetime\">";
            echo ($context["text_datetime"] ?? null);
            echo "</option>
                    ";
        }
        // line 99
        echo "                  </optgroup>
                </select>
              </div>
            </div>
            <div class=\"form-group row\">
              <label for=\"input-sort-order\" class=\"col-sm-2 col-form-label\">";
        // line 104
        echo ($context["entry_sort_order"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"sort_order\" value=\"";
        // line 106
        echo ($context["sort_order"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_sort_order"] ?? null);
        echo "\" id=\"input-sort-order\" class=\"form-control\"/>
              </div>
            </div>
          </fieldset>
          <fieldset>
            <legend>";
        // line 111
        echo ($context["text_value"] ?? null);
        echo "</legend>
            <table id=\"option-value\" class=\"table table-bordered table-hover\">
              <thead>
                <tr>
                  <td class=\"text-left required\">";
        // line 115
        echo ($context["entry_option_value"] ?? null);
        echo "</td>
                  <td class=\"text-center\">";
        // line 116
        echo ($context["entry_image"] ?? null);
        echo "</td>
                  <td class=\"text-right\">";
        // line 117
        echo ($context["entry_sort_order"] ?? null);
        echo "</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                ";
        // line 122
        $context["option_value_row"] = 0;
        // line 123
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["option_values"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
            // line 124
            echo "                  <tr id=\"option-value-row";
            echo ($context["option_value_row"] ?? null);
            echo "\">
                    <td class=\"text-center\"><input type=\"hidden\" name=\"option_value[";
            // line 125
            echo ($context["option_value_row"] ?? null);
            echo "][option_value_id]\" value=\"";
            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "option_value_id", [], "any", false, false, false, 125);
            echo "\"/>
                      ";
            // line 126
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 127
                echo "                        <div class=\"input-group\">
                          <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><img src=\"language/";
                // line 129
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 129);
                echo "/";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 129);
                echo ".png\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 129);
                echo "\"/></span>
                          </div>
                          <input type=\"text\" name=\"option_value[";
                // line 131
                echo ($context["option_value_row"] ?? null);
                echo "][option_value_description][";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 131);
                echo "][name]\" value=\"";
                echo (((($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = twig_get_attribute($this->env, $this->source, $context["option_value"], "option_value_description", [], "any", false, false, false, 131)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 131)] ?? null) : null)) ? (twig_get_attribute($this->env, $this->source, (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = twig_get_attribute($this->env, $this->source, $context["option_value"], "option_value_description", [], "any", false, false, false, 131)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 131)] ?? null) : null), "name", [], "any", false, false, false, 131)) : (""));
                echo "\" placeholder=\"";
                echo ($context["entry_option_value"] ?? null);
                echo "\" class=\"form-control\"/>
                        </div>
                        ";
                // line 133
                if ((($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["error_option_value"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52[($context["option_value_row"] ?? null)] ?? null) : null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 133)] ?? null) : null)) {
                    // line 134
                    echo "                          <div class=\"invalid-tooltip\">";
                    echo (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["error_option_value"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386[($context["option_value_row"] ?? null)] ?? null) : null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 134)] ?? null) : null);
                    echo "</div>
                        ";
                }
                // line 136
                echo "                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</td>
                    <td class=\"text-left\">
                      <div class=\"card image\">
                        <img src=\"";
            // line 139
            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "thumb", [], "any", false, false, false, 139);
            echo "\" alt=\"\" title=\"\" id=\"thumb-image";
            echo ($context["option_value_row"] ?? null);
            echo "\" data-placeholder=\"";
            echo ($context["placeholder"] ?? null);
            echo "\" class=\"card-img-top\"/> <input type=\"hidden\" name=\"option_value[";
            echo ($context["option_value_row"] ?? null);
            echo "][image]\" value=\"";
            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 139);
            echo "\" id=\"input-image";
            echo ($context["option_value_row"] ?? null);
            echo "\"/>
                        <div class=\"card-body\">
                          <button type=\"button\" data-toggle=\"image\" data-target=\"#input-image";
            // line 141
            echo ($context["option_value_row"] ?? null);
            echo "\" data-thumb=\"#thumb-image";
            echo ($context["option_value_row"] ?? null);
            echo "\" class=\"btn btn-primary btn-sm btn-block\"><i class=\"fas fa-pencil-alt\"></i> ";
            echo ($context["button_edit"] ?? null);
            echo "</button>
                          <button type=\"button\" data-toggle=\"clear\" data-target=\"#input-image";
            // line 142
            echo ($context["option_value_row"] ?? null);
            echo "\" data-thumb=\"#thumb-image";
            echo ($context["option_value_row"] ?? null);
            echo "\" class=\"btn btn-warning btn-sm btn-block\"><i class=\"fas fa-trash-alt\"></i> ";
            echo ($context["button_clear"] ?? null);
            echo "</button>
                        </div>
                      </div>
                    </td>
                    <td class=\"text-right\"><input type=\"text\" name=\"option_value[";
            // line 146
            echo ($context["option_value_row"] ?? null);
            echo "][sort_order]\" value=\"";
            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "sort_order", [], "any", false, false, false, 146);
            echo "\" class=\"form-control\" placeholder=\"";
            echo ($context["entry_sort_order"] ?? null);
            echo "\"/></td>
                    <td class=\"text-right\"><button type=\"button\" onclick=\"\$('#option-value-row";
            // line 147
            echo ($context["option_value_row"] ?? null);
            echo "').remove();\" data-toggle=\"tooltip\" title=\"";
            echo ($context["button_remove"] ?? null);
            echo "\" class=\"btn btn-danger\"><i class=\"fas fa-minus-circle\"></i></button></td>
                  </tr>
                  ";
            // line 149
            $context["option_value_row"] = (($context["option_value_row"] ?? null) + 1);
            // line 150
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 151
        echo "              </tbody>
              <tfoot>
                <tr>
                  <td colspan=\"3\"></td>
                  <td class=\"text-right\"><button type=\"button\" onclick=\"addOptionValue();\" data-toggle=\"tooltip\" title=\"";
        // line 155
        echo ($context["button_option_value_add"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fas fa-plus-circle\"></i></button></td>
                </tr>
              </tfoot>
            </table>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<script type=\"text/javascript\"><!--
\$('select[name=\\'type\\']').on('change', function() {
\tif (this.value == 'select' || this.value == 'radio' || this.value == 'checkbox' || this.value == 'image') {
\t\t\$('#option-value').parent().show();
\t} else {
\t\t\$('#option-value').parent().hide();
\t}
});

\$('select[name=\\'type\\']').trigger('change');

var option_value_row = ";
        // line 176
        echo ($context["option_value_row"] ?? null);
        echo ";

function addOptionValue() {
\thtml = '<tr id=\"option-value-row' + option_value_row + '\">';
\thtml += '  <td class=\"text-left\"><input type=\"hidden\" name=\"option_value[' + option_value_row + '][option_value_id]\" value=\"\" />';
  ";
        // line 181
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 182
            echo "\thtml += '    <div class=\"input-group\">';
\thtml += '      <div class=\"input-group-prepend\"><span class=\"input-group-text\"><img src=\"language/";
            // line 183
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 183);
            echo "/";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 183);
            echo ".png\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 183);
            echo "\" /></span></div>';
\thtml += '      <input type=\"text\" name=\"option_value[' + option_value_row + '][option_value_description][";
            // line 184
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 184);
            echo "][name]\" value=\"\" placeholder=\"";
            echo ($context["entry_option_value"] ?? null);
            echo "\" class=\"form-control\" />';
\thtml += '    </div>';
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 187
        echo "\thtml += '  </td>';
\thtml += '  <td class=\"text-center\">';
\thtml += '    <div class=\"card image\">';
\thtml += '      <img src=\"";
        // line 190
        echo ($context["placeholder"] ?? null);
        echo "\" alt=\"\" title=\"\" id=\"thumb-image' + option_value_row + '\" data-placeholder=\"";
        echo ($context["placeholder"] ?? null);
        echo "\" class=\"card-img-top\"/>';
\thtml += '      <input type=\"hidden\" name=\"option_value[' + option_value_row + '][image]\" value=\"\" id=\"input-image' + option_value_row + '\"/>';
\thtml += '      <div class=\"card-body\">';
\thtml += '        <button type=\"button\" data-toggle=\"image\" data-target=\"#input-image' + option_value_row + '\" data-thumb=\"#thumb-image' + option_value_row + '\" class=\"btn btn-primary btn-sm btn-block\"><i class=\"fas fa-pencil-alt\"></i> ";
        // line 193
        echo ($context["button_edit"] ?? null);
        echo "</button>';
\thtml += '        <button type=\"button\" data-toggle=\"clear\" data-target=\"#input-image' + option_value_row + '\" data-thumb=\"#thumb-image' + option_value_row + '\" class=\"btn btn-warning btn-sm btn-block\"><i class=\"fas fa-trash-alt\"></i> ";
        // line 194
        echo ($context["button_clear"] ?? null);
        echo "</button>';
\thtml += '      </div>';
\thtml += '    </div>';
\thtml += '  </td>';
\thtml += '  <td class=\"text-right\"><input type=\"text\" name=\"option_value[' + option_value_row + '][sort_order]\" value=\"\" placeholder=\"";
        // line 198
        echo ($context["entry_sort_order"] ?? null);
        echo "\" class=\"form-control\" /></td>';
\thtml += '  <td class=\"text-right\"><button type=\"button\" onclick=\"\$(\\'#option-value-row' + option_value_row + '\\').remove();\" data-toggle=\"tooltip\" title=\"";
        // line 199
        echo ($context["button_remove"] ?? null);
        echo "\" class=\"btn btn-danger\"><i class=\"fas fa-minus-circle\"></i></button></td>';
\thtml += '</tr>';

\t\$('#option-value tbody').append(html);

\toption_value_row++;
}
//--></script>
";
        // line 207
        echo ($context["footer"] ?? null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "catalog/option_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  590 => 207,  579 => 199,  575 => 198,  568 => 194,  564 => 193,  556 => 190,  551 => 187,  540 => 184,  532 => 183,  529 => 182,  525 => 181,  517 => 176,  493 => 155,  487 => 151,  481 => 150,  479 => 149,  472 => 147,  464 => 146,  453 => 142,  445 => 141,  430 => 139,  420 => 136,  414 => 134,  412 => 133,  401 => 131,  392 => 129,  388 => 127,  384 => 126,  378 => 125,  373 => 124,  368 => 123,  366 => 122,  358 => 117,  354 => 116,  350 => 115,  343 => 111,  333 => 106,  328 => 104,  321 => 99,  315 => 97,  309 => 95,  306 => 94,  300 => 92,  294 => 90,  291 => 89,  285 => 87,  279 => 85,  277 => 84,  273 => 83,  270 => 82,  264 => 80,  258 => 78,  256 => 77,  252 => 76,  249 => 75,  243 => 73,  237 => 71,  234 => 70,  228 => 68,  222 => 66,  220 => 65,  216 => 64,  213 => 63,  207 => 61,  201 => 59,  198 => 58,  192 => 56,  186 => 54,  183 => 53,  177 => 51,  171 => 49,  169 => 48,  165 => 47,  159 => 44,  149 => 41,  143 => 39,  141 => 38,  132 => 36,  123 => 34,  119 => 32,  115 => 31,  110 => 29,  105 => 27,  100 => 25,  95 => 23,  92 => 22,  84 => 18,  82 => 17,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "catalog/option_form.twig", "");
    }
}
